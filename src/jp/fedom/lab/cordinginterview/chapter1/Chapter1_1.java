package jp.fedom.lab.cordinginterview.chapter1;

import java.util.ArrayList;

/**
 * ある文字列が、すべてユニークである（重複する文字がない）かどうかを 判定するアルゴリズムを実装してください。
 * また、それを実装するのに新たなデータ構造が使えない場合、どのようにすればよいですか？
 */
public class Chapter1_1 {

	/**
	 * ある文字列が、すべてユニークである（重複する文字がない）かどうかを 判定するアルゴリズムを実装してください。
	 */
	public static boolean isUniqChar(String str) {
		if (str == null) {
			return false;
		}
		if (str.isEmpty()) {
			return true;
		}

		ArrayList<Character> containsList = new ArrayList<>();

		for (int i = 0; i < str.length(); i++) {
			if (containsList.contains(str.charAt(i))) {
				return false;
			} else {
				containsList.add(str.charAt(i));
			}
		}
		return true;
	}

	/**
	 *  また、それを実装するのに新たなデータ構造が使えない場合、どのようにすればよいですか？
	 */
	public static boolean isUniqChar2(String str) {
		if (str == null) {
			return false;
		}
		if (str.isEmpty()) {
			return true;
		}
		StringBuilder newStr = new StringBuilder(str);
		
		for (int i_1 = 0; i_1 < newStr.length(); i_1++) {
			for (int i_2 = i_1; i_2 < newStr.length(); i_2++) {
				if(newStr.charAt(i_1) > newStr.charAt(i_2)){
					char tmpChar = newStr.charAt(i_1); 
					newStr.setCharAt(i_2, newStr.charAt(i_1));
					newStr.setCharAt(i_1, tmpChar);
				}
			}
		}
		
		System.out.println(newStr);

		for (int i = 0; i < newStr.length() - 1; i++) {
			if (newStr.charAt(i) == newStr.charAt(i+1)) {
				return false;
			} 
		}
		return true;
	}

}