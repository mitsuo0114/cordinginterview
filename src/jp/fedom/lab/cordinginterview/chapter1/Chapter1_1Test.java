package jp.fedom.lab.cordinginterview.chapter1;

import static org.junit.Assert.*;

import org.junit.Test;

public class Chapter1_1Test {
	
	@Test
	public void testNormalString() {
		assertTrue(Chapter1_1.isUniqChar("abcdef"));
		assertTrue(Chapter1_1.isUniqChar2("abcdef"));
	}

	@Test
	public void emptyString() {
		assertTrue(Chapter1_1.isUniqChar(""));
		assertTrue(Chapter1_1.isUniqChar2(""));
	}

	@Test
	public void deplicatedString() {
		assertFalse(Chapter1_1.isUniqChar("abcdefa"));
		assertFalse(Chapter1_1.isUniqChar2("abcdefa"));
	}

	@Test
	public void deplicatedString_consective() {
		assertFalse(Chapter1_1.isUniqChar("abacdef"));
		assertFalse(Chapter1_1.isUniqChar2("abacdef"));
	}
}
